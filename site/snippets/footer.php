<?php
  // Parse Kirbytext to support dynamic year, but remove all HTML like paragraph tags:
  $copyrightDate = html::decode($site->copyright()->kirbytext())
?>

  <footer class="tc mt5 mb4 mt6-l mb5-l f5 f5-m f4-l" role="contentinfo">
      <div class="mh3">

        <div class="di-ns mb2"><?=$copyrightDate?></div>
        <div class="di-ns dn mh2">—</div> 

        <div class="di-ns mb2">Made with <div class="dib red heart">♥</div> and faith</div>
        <div class="di-ns dn mh2">—</div> 

        <div class="di-ns mb2">
          <a class="<?=cssLink()?>" href="<?= url('impressum')?>">Impressum</a> 
        </div>
        <div class="di-ns dn mh2">—</div> 

        <div class="di-ns mb2">
          <a class="<?=cssLink()?>" href="<?= url('datenschutz')?>">Datenschutz</a> 
        </div>
        <div class="di-ns dn mh2">—</div> 
        
        <div class="di-ns mb2">
          <a class="<?=cssLink()?>" href="<?= url('panel')?>">Login</a>
        </div>

      </div>
  </footer>

</body>
</html>
