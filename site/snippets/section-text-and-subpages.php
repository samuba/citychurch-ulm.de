<?php
// params: $section (the page)
?>

<section class="pv6-l pv5 ">

  <?php if (!$section->text()->empty()) : ?>
    <div class="center mw7  ph0-l  ph5-m  ph3 f3-l f4-m f5  kirbytext">
      <?= $section->text()->kirbytext() ?>
    </div>
  <?php endif ?>

  <div class="flex justify-center">
    <div class="mw9 flex justify-center flex-wrap">
      <?php snippet("subpages", ['children' => $section->children()->visible()]) ?>
    </div>
  </div>
  
</section>