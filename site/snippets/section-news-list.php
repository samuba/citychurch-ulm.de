<?php
    // params: $section (the page)
?>

<section class="pb5">
    <h2 class="tc  f2-ns pt5-ns pb3-ns  f3 pt4"><?= $section->title() ?></h2>

    <?php snippet('news-list', ['limit' => $section->limit()->int()]) ?>
</section>
