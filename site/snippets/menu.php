<nav class="flex-wrap ma-auto tc" role="navigation">
  <?php foreach($pages->visible() as $item): ?>
    <a href="<?=$item->url()?>" 
      title="<?=$item->title()->html()?>" 
      class="<?=r($item->isOpen(), 'red')?> 
            underline-hover link gray lh-big f5-l ph3-l f5-m ph3-m lh-title f6 fw6 ph2 mv1">
      <?= $item->title()->html() ?>    
    </a>
 
  <?php endforeach ?>
</nav>
