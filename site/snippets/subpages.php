<?php
    // params: $children (list of subpages to display)
?>

<?php foreach ($children as $child) : ?>
    <article class="w-100 h3p5 mv2  w5p5-ns h4-ns ma3-ns br2 inline-flex shadow-glow1">
        <a href="<?= $child->url() ?>" class="link br2 w-100 h-100 flex flex-column justify-center bg-center cover" 
            style="background-image: url('<?= getBannerImageUrl($child) ?>');">

            <h2 class="tc ma0 w-100 white t-shadow2 f3p5  f2-ns">
                <?= $child->title() ?>
            </h2>
        </a>
    </article>
<?php endforeach ?>