<main role="main" class="<?= cssContent() ?>">
      
    <?php snippet('event-facts') ?>

    <div class="tc">
      <div class="tl dib mt--1 kirbytext f3-l f4-m f5 ">
        <?= $page->text()->kirbytext() ?>
      </div>
    </div>

</main>