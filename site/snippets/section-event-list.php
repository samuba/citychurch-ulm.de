<?php
    // params: $section (the page)
?>

<section class="pt5-l pb6-l  pv5">
    <h2 class="tc  f2-ns pt5-ns pb3-ns  f3 pt4"><?= $section->title() ?></h2>

    <?php snippet("event-list", ['pageSize'=> $section->limit()->int()]) ?>

    <div class="tc pt4">
        <a href="<?= page("termine")->url() ?>" class="<?=cssButton()?>">
            <i class="fa fa-calendar"></i>&nbsp; Alle Termine
        </a>
    </div>
</section>
