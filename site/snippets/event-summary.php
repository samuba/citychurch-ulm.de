<?php 
    // params: $event
    
    $tileImage = getTileImageUrl($event);
    $date = formatDateStr($event->parent()->title(), '%A, %d.%m.%Y');
?>

<article class="ma3 br2 dit shadow-glow1">
    <a href="<?= $event->url() ?>" class="link h5 w5 br2 db" 
        style="background: url('<?= $tileImage ?>'); margin-bottom: -1px">

        <h2 class="f5 tc bg-white-70 ma0 pt3 db relative top top-30prct black">
            <?= $date ?>
        </h2>
        <h2 class="f3 tc bg-white-70 ma0 pb3 db relative top top-30prct white t-shadow1">
            <?= $event->title() ?>
        </h2>
    </a>
</article>