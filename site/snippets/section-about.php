<?php
    // params: $section (the page)
?>

<section class="flex justify-center mh0  flex-nowrap-l items-center-l pa5-l mv5-l  flex-wrap-m pa5-m mv5-m  flex-wrap pa3">
    
    <?php if ($section->hasImages()) : ?>
        <div class="flex-l w-30-l items-center-l justify-center-l mr5-l  w4-m mb3-m  w3 mt3">
            <?=$section->images()->first() ?>
        </div>
    <?php endif ?>

    <div class="lh-copy  w-70-l  f3-ns  kirbytext">
        <?= $section->text()->kirbytext() ?>
    </div>

</section>
