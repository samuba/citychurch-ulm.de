<!doctype html>
<html lang="<?= site()->language() ? site()->language()->code() : 'de' ?>">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">

  <title><?= $site->title()->html() ?> | <?= $page->title()->html() ?></title>
  <meta name="description" content="<?= $site->description()->html() ?>">
  
  <?= cssUncached('assets/css/tachyons.min.css') ?>
  <?= cssUncached('assets/css/index.css') ?>

  <link rel="alternate" type="application/rss+xml" title="Hör dir unsere Predigten an " href="<?=site()->find('predigten/podcast-feed')->url()?>" />

  <!-- favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicons/favicon-16x16.png">
  <link rel="manifest" href="/assets/favicons/site.webmanifest">
  <link rel="mask-icon" href="/assets/favicons/safari-pinned-tab.svg" color="#c00000">
  <link rel="shortcut icon" href="/assets/favicons/favicon.ico">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-config" content="/assets/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <!-- load icons async -->
  <link rel="stylesheet" href="<?=kirby()->urls()->assets()?>/css/font-awesome.min.css" media="none" onload="if(media!='all')media='all'">
  <noscript><link rel="stylesheet" href="<?=kirby()->urls()->assets()?>/css/font-awesome.min.css"></noscript>
</head>
<body>

  <header class="mw9 center bg-white pt2 pb3 pb2-l pb3-m " role="banner">
    <div class="flex-l">

      <a href="<?= url() ?>" class="ma-auto" rel="home">
        <img class="center db mw-none  h2p2-ns h2" src="<?=logoImageUrl()?>" >
      </a>   

      <?php snippet('menu') ?>

    </div>
  </header>