<?php
    // params: coverImage, headline, subtitle
?>
<div class="heroimage  flex flex-column justify-center items-center">
    <h1 class="mv0 mv1-ns white t-shadow1 f1-l f2-m f4">
        <?= $headline ?>
    </h1>
    <h2 class="mv0 white t-shadow1 f3-l f4-m f5 normal">
        <?= $subtitle ?>
    </h2>
</div>

<style>
@media screen and (max-width: 30em) { /* Smartphone */
    .heroimage {
        background-image: url('<?= thumb($coverImage, [
            'width' => 500, 
            'height' => 300, 
            'crop' => true, 
            'quality' => 80 ])->url(); ?>
        ');
    }
}

@media screen and (min-width: 30em) { /* not Smartphone */
    .heroimage {
        background-image: url('<?= thumb($coverImage, [
            'width' => 1920, 
            'height' => 600, 
            'crop' => true, 
            'quality' => 90 ])->url(); ?>
        ');
    }
}
</style>