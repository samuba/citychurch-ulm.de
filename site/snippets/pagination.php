<?php
  // params: 
  // $pagination (pagination object of a page)
  // $leftTitle (title of left button)
  // $rightTitle (title of right button)
?>

<?php if($pagination->hasPages()): ?>
  <nav class="overflow-hidden">

    <?php if($pagination->hasPrevPage()): ?>
      <a class="fl <?=cssButton()?>" href="<?= $pagination->prevPageURL() ?>" rel="prev" title="<?= $leftTitle ?>">
        🡨 <?= $leftTitle ?>
      </a>
    <?php endif ?>

    <?php if($pagination->hasNextPage()): ?>
      <a class="fr f1 <?=cssButton()?>" href="<?= $pagination->nextPageURL() ?>" rel="next" title="<?= $rightTitle ?>">
        <?= $rightTitle ?> 🡪
      </a>
    <?php endif ?>

  </nav>
<?php endif ?>