<?php 
    $date = formatDateStr($page->parent()->title(), '%A, %d.%m.%Y');
?>
<div class="tc-l tc-m pb4-l pb3-m pb2">
    <ul class="list tl dib f3-l f4-m f5 ma0">
        <li class="mv3p3" >
            <i class="fa fa-lg red fa-calendar"></i>&nbsp&nbsp<?=$date?>
        </li>
        <li class="mv3p3">
            <i class="fa fa-lg red fa-clock-o"></i>&nbsp&nbsp<?=$page->begin()->html()?> bis <?=$page->end()->html()?>
        </li>
        <li class="mv3p3">
            <i class="fa fa-lg red fa-map-marker pl1"></i>&nbsp&nbsp&nbsp<?=$page->location()->html()?>
        </li>
    </ul>
</div>