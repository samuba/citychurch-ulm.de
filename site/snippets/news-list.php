<?php
    // params: $limit (max items to output)

    $news = page('news')->children()
        ->visible()
        ->sortBy('sort', 'desc')
        ->limit($limit);

    function getThumbnail($newsItem) {
        return thumb($newsItem->image(), [
            'width' => 256, 
            'height' => 256, 
            'crop' => true, 
            'quality' => 78 ])->url();
    }
?>

<div class="tc">
    <?php foreach ($news as $newsItem) : ?>

        <article class="ma3 br2 dit shadow-glow1">
            <a href="<?= $newsItem->url() ?>" class="link h5 w5 br2 db" 
                style="background: url('<?= getThumbnail($newsItem) ?>'); margin-bottom: -1px">

                <h2 class="f3 tc bg-white-70 ma0 pv3 db relative top top-30prct white t-shadow1">
                    <?= $newsItem->title() ?>
                </h2>
            </a>
        </article>

    <?php endforeach ?>
</div>
