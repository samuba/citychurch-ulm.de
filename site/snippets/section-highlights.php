<?php
// params: $section (the page)
?>

<section class="flex justify-center flex-wrap ph3 pv5  flex-nowrap-l items-center-l  flex-wrap-m pa5-m">

    <div class="flex justify-center items-center flex-wrap  mw9-l flex-nowrap-l ph4-l">
        <?php if (!$section->img()->empty()) : ?>
            <?php if ($section->link()->isNotEmpty()) : ?>
                <a href="<?= $section->link() ?>">
            <?php endif ?>
                <img class="" src="<?= $section->img()->toFile()->url() ?>">
            </a>
        <?php endif ?>

        <?php if ($section->img()->isNotEmpty() && ($section->theTitle()->isNotEmpty() || $section->text()->isNotEmpty() )) : ?>
            <div class="mr5-l"></div>
        <?php endif ?>

        <div class=" ph0-l pt0-l pt4">
            <h2 class="mv0 f2"><?= $section->theTitle() ?></h2>
            <div class="lh-copy f3-ns mt-3 kirbytext">
                <?= $section->text()->kirbytext() ?>
            </div>

            <?php if ($section->link()->isNotEmpty()) : ?>
                <a href="<?= $section->link() ?>" class="<?= cssButton() ?>">
                    <?= $section->linkText() ?> &nbsp; <i class="fa fa-long-arrow-right"></i>
                </a>
            <?php endif ?>
        </div>
    </div>

</section>