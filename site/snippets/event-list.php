<?php
    // params: 
    // $pageSize (max items to show per page)
    // $showPagination (should pagination be shown)

    $pageSize = isset($pageSize) ? $pageSize : 3;
    $showPagination = isset($showPagination) ? $showPagination : false;

    $events = $pages->find("kalender") // structure example: kalender/2017/20-07-2017/my-event
        ->grandChildren() // get all days in year
        ->filter((function ($day) { // remove old ones
            return strtotime($day->title()) >= strtotime(date("Y-m-d"));
        }))
        ->children() // get events
        ->filterBy('public', 'true')
        ->visible()
        ->paginate($pageSize);
?>

<div class="center tc mw8">
    <?php foreach ($events as $event) : ?>
        <?php snippet("event-summary", ['event'=> $event]) ?>
    <?php endforeach ?>

    <?php if($showPagination): ?>
        <br><br><br>
        <?php snippet('pagination', [
            'pagination' => $events->pagination(), 
            'leftTitle' => 'Älter', 
            'rightTitle' => 'Neuer'
        ]) ?>
    <?php endif ?>
</div>
