<?php
return [
    'name'        => 'Kalender-Öffentlich-Schreiben',
    'default'     => false,
    'permissions' => [
        '*'                     => false,
        'panel.access'          => true,
        'panel.access.users'    => true,

        'panel.widget.pages'            => true,
        'panel.widget.account'          => true,
        'panel.widget.history'          => true,
        'panel.widget.site'             => true,
        'panel.widget.content-viewer'   => true,
                
        'panel.user.read'       => true,
        'panel.user.delete'     => function() { return isTargetUserHimself($this); },
        'panel.user.update'     => function() { return isTargetUserHimself($this); },
        'panel.avatar.upload'   => function() { return isTargetUserHimself($this); },
        'panel.avatar.replace'  => function() { return isTargetUserHimself($this); },
        'panel.avatar.delete'   => function() { return isTargetUserHimself($this); },

        'panel.page.read'       => function() { return isCalendarPage($this) || isTeamListPage($this); },
        'panel.page.create'     => function() { return isCalendarPage($this); },
        'panel.page.update'     => function() { return isCalendarPage($this) || isTeamListPage($this); },
        'panel.page.delete'     => function() { return isCalendarPage($this); },
        'panel.page.visibility' => function() { return isCalendarPage($this); },

        'panel.file.upload'  => function() { return isCalendarPage($this); },
        'panel.file.replace' => function() { return isCalendarPage($this); },
        'panel.file.rename'  => function() { return isCalendarPage($this); },
        'panel.file.update'  => function() { return isCalendarPage($this); },
        'panel.file.sort'    => function() { return isCalendarPage($this); },
        'panel.file.delete'  => function() { return isCalendarPage($this); },
    ]
];
