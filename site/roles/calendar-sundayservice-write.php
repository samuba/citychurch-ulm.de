<?php
return [
    'name'        => 'Kalender-Gottesdienst-Schreiben',
    'default'     => true,
    'permissions' => [
        '*'                     => false,
        'panel.access'          => true,
        'panel.access.users'    => true,

        'panel.widget.pages'            => true,
        'panel.widget.account'          => true,
        'panel.widget.history'          => true,
        'panel.widget.site'             => true,
        'panel.widget.content-viewer'   => true,
                
        'panel.user.read'       => true,
        'panel.user.delete'     => function() { return isTargetUserHimself($this); },
        'panel.user.update'     => function() { return isTargetUserHimself($this); },
        'panel.avatar.upload'   => function() { return isTargetUserHimself($this); },
        'panel.avatar.replace'  => function() { return isTargetUserHimself($this); },
        'panel.avatar.delete'   => function() { return isTargetUserHimself($this); },

        'panel.page.read'       => function() { return isCalendarPage($this)|| isTeamListPage($this); },
        'panel.page.create'     => function() { return isTargetNonPublicEvent($this); },
        'panel.page.update'     => function() { return !isPublicEvent($this) || isSundayService($this) || isTeamListPage($this); },
        'panel.page.delete'     => function() { return !isPublicEvent($this); },
        // dirty hack for: https://github.com/getkirby/panel/issues/989  should be: function() { return !isPublicEvent($this); },
        'panel.page.visibility' => function() { return true; },

        'panel.file.upload'  => function() { return !isPublicEvent($this) || isSundayService($this); },
        'panel.file.replace' => function() { return !isPublicEvent($this) || isSundayService($this); },
        'panel.file.rename'  => function() { return !isPublicEvent($this) || isSundayService($this); },
        'panel.file.update'  => function() { return !isPublicEvent($this) || isSundayService($this); },
        'panel.file.sort'    => function() { return !isPublicEvent($this) || isSundayService($this); },
        'panel.file.delete'  => function() { return !isPublicEvent($this) || isSundayService($this); },
    ]
];
