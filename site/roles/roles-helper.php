<?php

function isTeamListPage($that) {
    return $that->target()->page()->id() === $that->site()->find('teamliste')->id();
}

function isCalendarPage($that) {
    $calendarPage = $that->site()->find('kalender');
    return $that->target()->page()->isDescendantOf($calendarPage) || 
        $that->target()->page()->id() === $calendarPage->id();
}

function isPublicEvent($that) {
    return $that->target()->page()->public()->bool();
}

function isSundayService($that) {
    return $that->target()->page()->template() == 'event-sunday-service';
}

function isTargetNonPublicEvent($that) {
    if ($that->target()->blueprint() == null) return true;
    return $that->target()->blueprint() == 'event-generic';
}

function isTargetUserHimself($that) {
    return $that->user()->is($that->target()->user());
}