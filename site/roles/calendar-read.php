<?php
return [
    'name'        => 'Kalender-Lesen',
    'default'     => false,
    'permissions' => [
        '*'                     => false,
        'panel.access'          => true,
        'panel.access.users'    => true,

        'panel.widget.pages'            => true,
        'panel.widget.account'          => true,
        'panel.widget.history'          => true,
        'panel.widget.site'             => true,
        'panel.widget.content-viewer'   => true,
                
        'panel.user.read'       => true,
        'panel.user.delete'     => function() { return isTargetUserHimself($this); },
        'panel.user.update'     => function() { return isTargetUserHimself($this); },
        'panel.avatar.upload'   => function() { return isTargetUserHimself($this); },
        'panel.avatar.replace'  => function() { return isTargetUserHimself($this); },
        'panel.avatar.delete'   => function() { return isTargetUserHimself($this); },

        'panel.page.read'   => function() { return isCalendarPage($this) || isTeamListPage($this); },
    ]
];
