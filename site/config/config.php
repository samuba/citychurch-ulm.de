<?php

/* 
### Kirby Configuration (Docs: http://getkirby.com/docs/advanced/options) 
*/

// load license from file which is not in sourcecontrol
@include 'license.php';

// special debug-pages on errors
c::set('debug', true);

//c::set('thumbs.driver', 'im'); use ImageMagick for thumb generation etc

// custom css for panel
c::set('panel.stylesheet', '/assets/css/panel.css');

// default language is german
c::set('panel.language', 'de');

// logger plugin
c::set('logger.roles', ['admin']);
c::set('logger.entries', 600);


// custom helper functions (globally available)
include(dirname(__FILE__)."/../cssSnippets.php");
include(dirname(__FILE__)."/../helperFunctions.php");