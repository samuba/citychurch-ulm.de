<?php

function cssUncached($cssFile) {
    return css($cssFile.'?v='.filemtime($cssFile));
} 

function setDefaultLocale() { 
    // needs to be set for some reason before we output dates and stuff EVERY time
    setlocale(LC_ALL, 'de_DE.utf8');
}

function formatDateStr($str, $format) {
    setDefaultLocale();
    return strftime($format, strtotime($str));
}

function eventDefaultImage() { 
    return assetFromUrl(kirby()->urls()->index().'/assets/images/event.jpg'); 
}

function sundayServiceDefaultImage() { 
    return assetFromUrl(kirby()->urls()->index()."/assets/images/sundayservice.jpg");
}

function podcastDefaultImage() { 
    return assetFromUrl(kirby()->urls()->index().'/assets/images/podcast.jpg'); 
}

function logoImageUrl() { 
    return kirby()->urls()->index()."/assets/images/logo.png";
}

function fieldFileOrDefault($field, $default) {
    if($field->empty()) {
        return $default;
    } else {
        return $field->toFile();
    }
}

function assetFromUrl($urlToFile) {
    return new Asset(str_replace(kirby()->urls()->index().'/', "", $urlToFile));
}

function buildTileUrlFromImage($imageUrl) {
    return thumb($imageUrl, [
        'width' => 256, 
        'height' => 256, 
        'crop' => true, 
        'quality' => 80 ])->url();
}

function assertCurrentUserIsLoggedIn($site) {
    if(!$site->user()) go("panel/login");
}

function getTileImageUrl($page) {
    if($page->tileImage()->isNotEmpty()) {
        return buildTileUrlFromImage($page->tileImage()->toFile());
    } 
    if($page->coverImage()->isNotEmpty()) {
        return buildTileUrlFromImage($page->coverImage()->toFile());
    } 
    if ($page->template() == 'event-sunday-service') {
        return buildTileUrlFromImage(sundayServiceDefaultImage());
    } 
    return buildTileUrlFromImage(eventDefaultImage());
}

function printToServerLog($obj) {
    ob_start(); 
    var_dump($obj); 
    error_log(ob_get_clean(), 4);
}

function bannerImageUrlFromImage($imageUrl) {
  return thumb($imageUrl, [
    'width' => 385,
    'height' => 128,
    'crop' => true,
    'quality' => 90,
  ])->url();
}

function getBannerImageUrl($page) {
  if ($page->tileImage()->isNotEmpty()) {
    return bannerImageUrlFromImage($page->tileImage()->toFile());
  }
  if ($page->coverImage()->isNotEmpty()) {
    return bannerImageUrlFromImage($page->coverImage()->toFile());
  }
  return bannerImageUrlFromImage(eventDefaultImage());
}