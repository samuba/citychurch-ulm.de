<?php
  if ($page->intro()->isNotEmpty()) {
    $subtitle = '<br>' . $page->intro() . '<br>' . $page->series();
  } else {
    $subtitle = $page->series()->kirbytext();
  }
?>

<?php snippet('header') ?>

  <?php snippet('hero-header', [
    'coverImage' => fieldFileOrDefault($page->coverImage(), sundayServiceDefaultImage()),
    'headline' => $page->title()->html(),
    'subtitle' => $subtitle
  ]) ?>

  <?php snippet('event-content') ?>

<?php snippet('footer') ?>


