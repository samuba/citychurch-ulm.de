<?php 
  $coverImage = fieldFileOrDefault($page->coverImage(), eventDefaultImage());
?>

<?php snippet('header') ?>

  <?php snippet('hero-header', [
    'coverImage' => $coverImage,
    'headline' => $page->intro()->kirbytext(),
    'subtitle' => ""
  ]) ?>  


  <main class="bg-even-child-sections-gray" role="main">

    <?php foreach($page->children()->visible() as $section): ?>
      <?php snippet($section->intendedTemplate(), ['section' => $section]) ?>
    <?php endforeach ?>

  </main>

<?php snippet('footer') ?>