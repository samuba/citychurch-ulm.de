<?php snippet('header') ?>

    <?php snippet('hero-header', [
        'coverImage' => $page->coverImage()->toFile(),
        'headline' => $page->title()->html(),
        'subtitle' => $page->intro()->kirbytext()
    ]) ?>
    <main role="main">

        <div class="<?= cssContent() ?> mb4">
            <div class="tc kirbytext  f3-l  f4-m  f5">
                <?= $page->text()->kirbytext() ?>
            </div>
            <?php if ($site->user()) : ?>
                <div class="tc">
                    <a href="panel/pages/kalender/edit" class="<?=cssButton()?>">Kalender</a>
                    <a href="<?=$site->find('gottesdienst-planung')?>" class="<?=cssButton()?>">Gottesdienst Planung</a>
                </div>
            <?php endif ?>
        </div>

        <?php snippet("event-list", ['pageSize'=> $page->perpage()->int(), 'showPagination'=> true]) ?>
    </main>

<?php snippet('footer') ?>
