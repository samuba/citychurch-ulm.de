<?php snippet('header') ?>

  <header class="flex justify-center  header-gradient  pv5-ns  pv3">
    <h2 class="white t-shadow1 tc mv3  f2-l  f3-m f4 "> <?=$page->title()->html()?> </h2>
  </header>

  <main class="center mw7  pt4-l ph0-l  ph5-m pt4-m  ph3 pt3" role="main">
      
    <h3 class="gray i f5-ns  mt3-l pt4-l  mt3-m pt2m   f6">
      <?=$page->date('F jS, Y')?>
    </h3>

    <div class=" f4-ns  f5  kirbytext">
      <?= $page->text()->kirbytext() ?>
    </div>

    <div class="flex flex-wrap justify-center mt5-l mt4-m dib">
      <?php foreach ($page->images() as $image): ?>
        <a href="<?= $image->url() ?>" class="ma3 dib">
          <img class="shadow-glow1 h4p5 mw-none"
            src="<?= thumb($image, array('height' => 210))->url() ?>">
        </a>
      <?php endforeach ?>
    </div>
    
  </main>

<?php snippet('footer') ?>