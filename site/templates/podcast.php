<?php
  $perpage = $page->perpage()->int();
  $sundayservices = $pages->find("kalender") // structure example: kalender/2017/20-07-2017/my-event
    ->grandChildren() // get all days in year
    ->children() // get events
    ->filter((function ($event) { // only sundayservices with audio files
      return $event->template() == "event-sunday-service" && $event->hasAudio();
    }))
    ->flip()
    ->paginate(($perpage >= 1)? $perpage : 8);

  function prettyDate($dateStr) {
    return date_format(date_create($dateStr), "d.m.Y");
  }
?>

<?php snippet('header') ?>

  <?php snippet('hero-header', [
    'coverImage' => fieldFileOrDefault($page->coverImage(), podcastDefaultImage()),
    'headline' => $page->title()->html(),
    'subtitle' => $page->intro()->kirbytext()
  ]) ?>

  <main class="<?=cssContent()?> flex flex-column items-center f3-l f4-m f5" role="main">

    <div class="mb3  kirbytext">
        <span class="tc"><?= $page->text()->kirbytext() ?></span>
    </div>

    <div >
      <?php foreach($sundayservices as $sunday): ?>
        <article class="mv3 pv3 lh-copy">
          <?php if($sunday->series()->isNotEmpty()): ?>
            <span class="i gray"><?= $sunday->series() ?></span><br>
          <?php endif ?>
          <?php if($sunday->intro()->isNotEmpty()): ?>
            <span><?= $sunday->intro() ?></span><br>
          <?php endif ?>

          <?php if($sunday->videoLink()->isNotEmpty()): ?>
            <a href="<?=$sunday->videoLink()?>" title="Video" class="<?=cssLink()?> pr2"><i class="fa fa-tv"></i></a> 
          <?php endif ?>

          <a href="<?=$sunday->audio()->first()->url()?>" title="Audio" class="<?=cssLink()?> pr2"><i class="fa fa-volume-up"></i></a> 

          <?php if($sunday->hasDocuments()): ?>
            <a href="<?=$sunday->documents()->first()->url()?>" title="Dokument" class="<?=cssLink()?> pr2"><i class="fa fa-file-text-o"></i></a> 
          <?php endif ?>

          <span><?= prettyDate($sunday->parent()->title()) ?> — </span>
          <span class=" i"><?= $sunday->sermon() ?></span><br>
        </article>
      <?php endforeach ?>

      <?php snippet('pagination', [
        'pagination' => $sundayservices->pagination(), 
        'leftTitle' => 'Neuer', 
        'rightTitle' => 'Älter'
        ]) ?>
    </div>

  </main>

<?php snippet('footer') ?>