<?php snippet('header') ?>

  <?php snippet('hero-header', [
    'coverImage' => fieldFileOrDefault($page->coverImage(), eventDefaultImage()),
    'headline' => $page->title()->html(),
    'subtitle' => $page->intro()->kirbytext()
  ]) ?>

  <?php snippet('event-content') ?>

<?php snippet('footer') ?>