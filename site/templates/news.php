<?php
  $perpage = $page->perpage()->int();
  $articles = $page->children()
                   ->visible()
                   ->flip()
                   ->paginate(($perpage >= 1)? $perpage : 5);

  function getNewsArticleThumbnail($article) {
    return thumb($article->images()->first(), [
      'width' => 800, 
      'height' => 300, 
      'crop' => true, 
      'quality' => 78 ]);
  }
?>


<?php snippet('header') ?>

  <?php snippet('hero-header', [
    'coverImage' => fieldFileOrDefault($page->coverImage(), eventDefaultImage()),
    'headline' => $page->title()->html(),
    'subtitle' => $page->intro()->kirbytext()
  ]) ?>

  <main class="center mw7  pt4-l ph0-l  ph5-m pt4-m  ph3 pt3" role="main">

    <section class="">
        <?php foreach($articles as $article): ?>

          <article class="mb5">
            <header>
                <a href="<?= $article->url() ?>" class="link">
                  <h2 class="red mb1  f3-ns  f4"> <?=$article->title()->html()?> </h2>
                  <span class="gray i  f4-ns  f5"> <?=$article->date('F jS, Y')?> </span>
                </a>
            </header>

           <?= /*getNewsArticleThumbnail($article)*/ "" ?>
     
            <p class="f4-ns  f5 ">
              <?= $article->text()->kirbytext()->excerpt(50, 'words') ?>
              <a href="<?= $article->url() ?>" class="link red">weiterlesen</a>
            </p>
          </article>

        <?php endforeach ?>
    </section>

    <?php snippet('pagination', [
      'pagination' => $articles->pagination(),
      'leftTitle' => 'Älter', 
      'rightTitle' => 'Neuer'  
    ]) ?>

  </main>

<?php snippet('footer') ?>