<?php
  $sundayservices = $pages->find("kalender") // structure example: kalender/2017/20-07-2017/my-event
  ->grandChildren() // get all days in year
  ->children() // get events
  ->filter((function ($event) { // only sundayservices with audio files
    return $event->template() == "event-sunday-service" && $event->hasAudio();
  }))
  ->flip();

  function dayToPubDate($dayStr) {
    return date('r', date_timestamp_get(date_create($dayStr)));
  }

  function buildTitle(&$sunday) {
    if ($sunday->series()->isEmpty()) {
      return sanitize($sunday->intro());
    }
    if ($sunday->intro()->isEmpty()) {
      return sanitize($sunday->series());
    }
    return sanitize($sunday->intro())." (".sanitize($sunday->series()).")";
  }

  // Needed because xml and rss have problems with nested html and chars like '&'.
  // This has to be bulletproof otherwise rss feed generation will fail silently.
  function sanitize($field) {
    /* $text = preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $field->kt()); // remove html tags including their content */
    $text = preg_replace("/&#?[a-z0-9]{2,8};/i","", $field->kt()); // remove html entities like $nbsp;
    return strip_tags($text); // just to be extra sure. Maybe I made an error in the regex.
  }
?>
<?php header("Content-type: text/xml"); ?>
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:googleplay="http://www.google.com/schemas/play-podcasts/1.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
   <channel>
      <title><?=sanitize($page->channelTitle())?></title>
      <googleplay:author><?=sanitize($page->author())?></googleplay:author>
      <itunes:author><?=sanitize($page->author())?></itunes:author>
      <googleplay:category text="Religion &amp; Spirituality">
        <googleplay:category text="Christianity"/>
      </googleplay:category>
      <itunes:category text="Religion &amp; Spirituality">
        <itunes:category text="Christianity"/>
      </itunes:category>
      <googleplay:email><?=$page->email()?></googleplay:email>
      <itunes:email><?=$page->email()?></itunes:email>
      <itunes:owner>
        <itunes:email><?=$page->email()?></itunes:email>
        <itunes:name><?=sanitize($page->author())?></itunes:name>
      </itunes:owner>
      <link><?=sanitize($page->link())?></link>
      <description><?=sanitize($page->description())?></description>
      <googleplay:description><?=sanitize($page->description())?></googleplay:description>
      <itunes:summary><?=sanitize($page->description())?></itunes:summary>
      <image>
        <link><?=sanitize($page->link())?></link>
        <title><?=sanitize($page->channelTitle())?></title>
        <url><?=$page->image()->url()?></url>
      </image>
      <googleplay:image href="<?=$page->image()->url()?>" />
      <itunes:image href="<?=$page->image()->url()?>" />
      <language><?=$page->language()?></language>
      <ttl><?=$page->ttl()?></ttl>
      <itunes:explicit>no</itunes:explicit>
      <googleplay:explicit>no</googleplay:explicit>
      <?php foreach($sundayservices as $sunday): ?>
        <item>
          <title><?=buildTitle($sunday)?></title>
          <itunes:title><?=buildTitle($sunday)?></itunes:title>
          <googleplay:author><?=$page->author()?></googleplay:author>
          <itunes:author><?=$page->author()?></itunes:author>
          <link><?=$sunday->audio()->first()->url()?></link>
          <enclosure url="<?=$sunday->audio()->first()->url()?>" type="audio/x-mp3" length="<?=$sunday->audio()->first()->size()?>" />
          <pubDate><?=dayToPubDate($sunday->parent()->title())?></pubDate>
          <guid><?=$sunday->audio()->first()->url()?></guid>
          <description><?=sanitize($sunday->text())?></description>
          <itunes:summary><?=sanitize($sunday->text())?></itunes:summary>
          <googleplay:description><?=sanitize($sunday->text())?></googleplay:description>
        </item>
      <?php endforeach ?>
   </channel>
</rss>
