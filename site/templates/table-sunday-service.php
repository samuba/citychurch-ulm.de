<?php
    assertCurrentUserIsLoggedIn($site);

    $godis = $pages->find("kalender")
        ->grandChildren()
        ->children()
        ->filterBy('template', 'event-sunday-service')
        ->flip()
        ->limit($page->limit()->int());
    
    function isInPast($godi) {
        $hours13 = 60*60*13; // godi should be active until afternoon
        return (time() - $hours13) > strtotime($godi->parent()->title());
    }
?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <div class="tc mb4">
        <h1><?= $page->title()->html() ?></h1>
        <div><?= $page->text()->kirbytext() ?></div>
        <a href="panel/pages/kalender/edit" class="<?=cssButton()?>">Kalender</a>
        <a href="panel/pages/teamliste/edit" class="<?=cssButton()?>">Teamliste</a>
    </div>

    <table class="f7 w-100 mw8 center ph2" cellspacing="0" id="godi-table">
        <thead>
            <tr id="godi-planung-head">
            <th class="fw6 bb    br b--black-20 tl pv1 pl1 w1 bg-white">
                <a href="javascript:showAllColumns()" title="Alle Spalten anzeigen" class="red link"><i class="fa fa-2x fa-eye"></i></a>
            </th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 w1 bg-white">Datum</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Thema</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Orga/Planung</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Ort</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Moderation</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Predigt</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Lobpreis</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Auf-/Abbau</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Technik</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Projektion</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Kids-Minis</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Kids-Maxis</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Taxis</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Gebet</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Catering</th>
            <th class="fw6 bb bt br b--black-20 tl pv1 pl1 min-w7rem bg-white">Welcome</th>
            </tr>
        </thead>

        <tbody class="lh-copy">
            <?php foreach ($godis as $godi) : ?>
                <tr id="<?=formatDateStr($godi->parent()->title(), "%d.%m.%Y")?>">
                <td class="pa1 bb br bl b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>">
                    <a href="panel/pages/<?=$godi->uri()?>/edit" title="Diesen Gottesdient bearbeiten" class="red link">
                        <i class="fa fa-2x fa-edit"></i>
                    </a>
                </td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=formatDateStr($godi->parent()->title(), "%d.%m.%Y")?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->title()?><br><?=$godi->series()?><br><?=$godi->intro()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->organisation()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->location()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->moderation()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->sermon()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->worship()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->buildup()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->technic()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->projection()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->kidsMinis()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->kidsMaxis()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->taxis()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->prayer()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->catering()?></td>
                <td class="pa1 bb br b--black-20 <?php e(isInPast($godi), 'bg-black-10') ?>"><?=$godi->welcome()?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

  </main>

<?php snippet('footer') ?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/floatthead/2.0.3/jquery.floatThead.min.js"></script>

<script>
var hiddenColumns = "hiddenGodiPlanungColumns"

function enableFloatingHeaderPlugin() { $('#godi-table').floatThead() }

function refreshFloatingHeader() { $('#godi-table').floatThead('reflow') }

function scrollToUpcomingEvent() { 
    var today = new Date()
    jQuery.fn.reverse = [].reverse; // inject reverse function into jquery
    $("#godi-table tbody tr").reverse().each(function(_, entry){
        var dateArray = entry.id.split(".")
        var godiDate = new Date(dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0])
        if (godiDate > today) {
            var aTag = $("#"+ entry.id.replace(/\./g, "\\."));
            $('html,body').animate({scrollTop: aTag.offset().top - 200},'slow');
            return false
        }
    })
}

function insertHideColumnButtons() {
    let heads = document.getElementById("godi-planung-head").children
    for(let i=2; i<=heads.length-1; i++) {
        var columnName = heads[i].innerHTML
        heads[i].innerHTML = columnName + ' <div class="dib"><a href="javascript:hideColumn(\'' + columnName + '\')" title="Spalte verstecken" class="red link"><i class="fa fa-eye-slash"></i></a></div>'
    }
}

function hideColumn(name) {
    let columns = JSON.parse(localStorage[hiddenColumns] || "[]")
    columns.push(name)
    localStorage.setItem(hiddenColumns, JSON.stringify(columns))
    showColumnElements(name, false)
}

function showColumnElements(name, show) {
    for(let th of document.querySelectorAll("th")) {
        if (th.innerHTML.indexOf(name) == -1) continue
        if (show) th.style.display = 'table-cell' 
        else th.style.display = 'none';
        var index = th.cellIndex
    }
    for(let td of document.querySelectorAll("td")) {
        if (td.cellIndex == index) {
            if (show) td.style.display = 'table-cell' 
            else td.style.display = 'none';
        }
    }
    refreshFloatingHeader()
} 

function hideColumnsThatShouldBeHidden() {
    for(let column of JSON.parse(localStorage[hiddenColumns] || "[]")) {
        showColumnElements(column, false)
    }
}

function showAllColumns() { 
    for(let column of JSON.parse(localStorage[hiddenColumns] || "[]")) {
        showColumnElements(column, true)
    }
    localStorage.setItem(hiddenColumns, "[]") 
}

enableFloatingHeaderPlugin()
hideColumnsThatShouldBeHidden()
insertHideColumnButtons()
scrollToUpcomingEvent()
</script>