<?php
if ($page->id() === $site->find('teamliste')->id()) {
  assertCurrentUserIsLoggedIn($site);
}
?>

<?php snippet('header') ?>

  <?php snippet('hero-header', [
    'coverImage' => fieldFileOrDefault($page->coverImage(), eventDefaultImage()),
    'headline' => $page->title()->html(),
    'subtitle' => $page->intro()->kirbytext()
  ]) ?>

  <main role="main">

    <div class="<?=cssContent()?> f3-l f4-m f5 mt--1  kirbytext">
      <?= $page->text()->kirbytext() ?>
    </div>

    <div class="center flex justify-center flex-wrap pt0 mw8 mt5-l  mt4-m  mt3">
      <?php snippet("subpages", ['children'=> $page->children()->visible()]) ?>
    </div>

  </main>

<?php snippet('footer') ?>