function builtCalendarBoard(M,Y){
    
    var M = M || $("#calendarboard").attr("data-month");
    var Y = Y || $("#calendarboard").attr("data-year");   
    
    var field_name = $("#calendarboard").attr("data-name");
    
    var _url = window.location.href;
    _url = _url.replace("/edit", "/field");
    _url += "/" + field_name + "/calendarboard/";
    
    $.ajax({
        url: _url + "get-month-board/" + M + "/" + Y,
        type: 'GET',
        success: function(board) {
          $("#calendarboard").html(board);         
        }
    });    
}

function makeCalendarSpanEntireView() {
  $(".mainbar").css('width', '100%')
  $(".sidebar").remove()
  $(".bars-with-sidebar-left").removeClass("bars-with-sidebar-left")
}

(function($) {
  $.fn.createCalendar = function() {
    makeCalendarSpanEntireView()

    return builtCalendarBoard();
  }

}(jQuery));