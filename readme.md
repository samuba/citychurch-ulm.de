# Website for citychurch-ulm
It's based on the awesome Kirby CMS and can be easily adapted for other churches or event oriented organizations.

Church websites and the likes tend to be maintained by multiple people and there is naturally fluctuation in the staff. Therefore everything needs to be as easy as possible in order to be quickly and easyly learned by a variety of people.
TODO: no assumptions of the internal church stucture/organization -> differs over time anyway, adaptable for other churches
That principal drove all technological and organizational decisions for this project.

Trimming everything to be easy also has other benefits:
* actually be used (complicated stuff is not used)
* bring actual benefit (if eg. organizing your worship song lineup is easier by using email, use email)

## Features
* Easy Setup
* Easy to use for church staff
* Easy Podcast management
* Easily maintainable
* Easily extendable
* Beautiful & simple Design
* Responsive
* Comprehensive Event organization
* Comprehensive Sundayservice duties organization


## Dependencies
* Styling: http://tachyons.io/
* Calendar View in Panel: https://github.com/molocLab/kirby-calendar-board
* Simple Audit Log in Panel: https://github.com/texnixe/kirby-logger

## Modifications from vanilla Kirby:

* No usage of kirby controllers (Logic lives at top of template file)
  * Because Logic is most of the time tightly coupled to template
  * Having both in one file is easier for mental model 
  * no back and forth between files 
  * still no mixup of Logic & HTML because Logic lives at top of file
  * Idea borrowed from Single-File-Components alá Vue, React etc

* functional style kirby snippets
  * snippets try to be "pure" - no dependencies/sideeffects on things except for params or kirby global stuff
  * snippets always state their params + types in comment at top of file

* Tachyons for Styling
  * Because it's awesome
  * Easy to change X without breaking Y on another page
  * TODO: seperation style  in classes
  * TODO: cssSnippets.php

TODO: helperFuntions.php